package kku.coe.webservice;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

@WebServlet(name = "XMLFileWriter", urlPatterns = {"/Pattira-XMLFileWriter"})
public class XMLFileWriter extends HttpServlet {

    public static void main(String argv[]) {

        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element root = doc.createElement("quotes");
            doc.appendChild(root);

            Element quote1 = doc.createElement("quote");
            root.appendChild(quote1);
            Element words1 = doc.createElement("words");
            Text tw1 = doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time.");
            words1.appendChild(tw1);
            quote1.appendChild(words1);
            Element by1 = doc.createElement("by");
            Text tb1 = doc.createTextNode("Jim Rohn");
            by1.appendChild(tb1);
            quote1.appendChild(by1);

            Element quote2 = doc.createElement("quote");
            root.appendChild(quote2);
            Element words2 = doc.createElement("words");
            Text tw2 = doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            words2.appendChild(tw2);
            quote2.appendChild(words2);
            Element by2 = doc.createElement("by");
            Text tb2 = doc.createTextNode("ว. วชิรเมธี");
            by2.appendChild(tb2);
            quote2.appendChild(by2);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("quotes.xml"));

            trans.transform(source, result);

            System.out.println("File saved!");

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}