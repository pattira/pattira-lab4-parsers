package kku.coe.webservice;

import java.io.*;
import java.net.URL;
import java.util.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.xml.parsers.*;
import org.w3c.dom.*;

@WebServlet(name = "XMLFileSearcher", urlPatterns = {"/Pattira-XMLFileSearcher"})
public class XMLFileSearcher {

    public static void main(String argv[]) throws FileNotFoundException {
        String keyword;
        String key = "keyword.txt";
        File xmlfile = new File("quotes.xml");
        Scanner scanner = new Scanner(new FileInputStream(key));
        
        try {
            while (scanner.hasNextLine()) {
                keyword = scanner.nextLine();
             
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = factory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlfile);
                
                NodeList items = doc.getElementsByTagName("quote");
                
                for (int i = 0; i < items.getLength(); i++) {
                    Element item = (Element) items.item(i);
                    if (getElemVal(item, "by").toLowerCase().contains(keyword.toLowerCase())) {
                        System.out.println(getElemVal(item, "words") + " by " + getElemVal(item, "by"));
                    }
                }
            }
        } catch (Exception e) {
            System.out.print(e);
        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}