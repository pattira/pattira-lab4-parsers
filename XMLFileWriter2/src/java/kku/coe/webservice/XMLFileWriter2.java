package kku.coe.webservice;

import java.io.FileWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

@WebServlet(name = "XMLFileWriter2", urlPatterns = {"/Pattira-XMLFileWriter2"})
public class XMLFileWriter2 extends HttpServlet {

    public static void main(String[] args) {
        try {
            String fileName = "quotes.xml";
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = null;
            xtw = xof.createXMLStreamWriter(new FileWriter(fileName));
            xtw.writeStartDocument("utf-8", "1.0");
            xtw.writeStartElement("quotes");
            
            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time.");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("Jim Rohn");
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("ว. วชิรเมธี");
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.writeEndElement();
            xtw.flush();
            xtw.close();
        } catch (Exception ex) {
            System.err.println("Exception occurred while running writer samples");
        }
        System.out.println("Done");
    }
}