package kku.coe.webservice;

import java.io.*;
import java.util.Scanner;
import javax.servlet.annotation.WebServlet;
import javax.xml.stream.*;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

@WebServlet(name = "XMLFileSearcher2", urlPatterns = {"/Pattira-XMLFileSearcher2"})
public class XMLFileSearcher2 {

    public static void main(String argv[]) throws FileNotFoundException, XMLStreamException {
        String key = "keyword.txt";
        File xmlfile = new File("quotes.xml");
        boolean qouteFound = false;
        boolean wordFound = false;
        boolean byFound = false;
        String keyword = null;
        String word = null;
        String by = null;
        String eName = null;
        Scanner scanner = new Scanner(new FileInputStream(key), "UTF-8");

        try {
            while (scanner.hasNextLine()) {
                keyword = scanner.nextLine();
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(xmlfile)));

                while (reader.hasNext()) {
                    XMLEvent event = reader.nextEvent();

                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            qouteFound = true;
                        }
                        if (qouteFound && eName.equals("words")) {
                            wordFound = true;
                        }
                        if (qouteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }

                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            qouteFound = false;
                        }
                        if (qouteFound && eName.equals("words")) {
                            wordFound = false;
                        }
                        if (qouteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }

                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;

                        if (byFound) {
                            by = characters.getData();

                            if (by.toLowerCase().contains(keyword.toLowerCase())) {
                                // 
                                System.out.println(word + " by " + by);
                            }
                        }
                        if (wordFound) {
                            word = characters.getData();
                        }
                    }
                }
            }

        } finally {
            scanner.close();
        }
    }
}